﻿using System;

namespace MvvmLight1.Model
{
    public class DataItem
    {
        public string Title
        {
            get { return "sa"; }
            set
            {
                if(value == null)
                {
                    throw new ArgumentNullException("value");
                }
            }
        }

        public DataItem(string title)
        {
            Title = title;
        }
    }
}