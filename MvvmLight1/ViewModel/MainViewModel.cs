﻿using System;
using System.Collections.Generic;
using System.Windows;
using GalaSoft.MvvmLight;
using MvvmLight1.Model;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Input;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace MvvmLight1.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class MainViewModel : ObservableObject
    {
        private readonly IDataService _dataService;
        private string _title = "24/7";
        private string _folderForSave ="C://";
        private string _welcomeTitle = string.Empty;
        private int _countThreads = 0;
        internal string fileName;
        public bool ListLinNotEmprty { get; set; } = true;
        public string WelcomeTitle { get { return _welcomeTitle; } set { Set(ref _welcomeTitle, value); } }
        public int CountThreads { get { return _countThreads; } set { Set(ref _countThreads, value); } }

        public string Title { get { return _title; } set { Set(ref _title, value); } }
        public string FolderForSave { get { return _folderForSave; }
            set
            {
                Set(ref _folderForSave, value);
            } }
        private string _textBoxContent = "http://rouslan.com/2011/06/03/mvvm-light-toolkit-commands-and-messenger/";

        public String TextBoxContent
        {
            get { return _textBoxContent; }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    ListLinNotEmprty = true;
                    CommandManager.InvalidateRequerySuggested();
                }
                Set(ref _textBoxContent, value);
            }
        }

        private List<string> _links;
        public List<string> Links { get { return _links; } set { Set(ref _links, value); } }

        public MainViewModel(IDataService dataService)
        {
            FileOpenComand = new Command(OpenFile());
            Start = new Command(StartCr());
            FolderFialog = new Command(SelectFolder());
            ThreadsStop = new Command(ThreadStop());

            _dataService = dataService;
            _dataService.GetData((item, error) =>
            {
                if(error != null)
                {
                    // Report error here
                    return;
                }
                WelcomeTitle = item.Title;
            });
        }


        public Command FileOpenComand { get; set; }
        public Command Start { get; set; }
        public Command FolderFialog { get; set; }
        public Command ThreadsStop{get; set;}
        private string[] baseLinks;

        public Action<Object> OpenFile()
        {
            return s =>
            {
                var dialog = new OpenFileDialog();
                dialog.ShowDialog();
                fileName = dialog.FileName;
                baseLinks = File.ReadAllLines(fileName);
                TextBoxContent = string.Join("\r", baseLinks);
            };
        }

        private void Proc(Object obj)
        {
            Stack<string> stack = new Stack<string>();
            Crawler.Crawler crawler = new Crawler.Crawler();
            //string[] baseLinks = TextBoxContent.Split('\r');
            List<string> links =
                    crawler.GetLinksFromUrl((string)obj).Where( el=>new Uri(el).Authority == new Uri((string)obj).Authority).ToList(); //baseLinks[0]);

            foreach(var item in links)
            {
                stack.Push(item);
            }
            int robotDelay = crawler.GetTimeRobotTxt(links[0]);
            while(stack.Any())
            {
                var popUrl = stack.Pop();
                KeyValuePair<string, string> html = crawler.GetHtmlFromUrl(popUrl, false);//Выкачиваем документ, без изменений
                    Thread.Sleep(robotDelay * 1000);
                Dictionary<string, string> csss = crawler.GetCssFromUrl(popUrl, html.Value);//Получаем CSS из уже скаченного документа
                    Thread.Sleep(robotDelay * 1000);
                Dictionary<string, string> jss = crawler.GetJsFromUrl(popUrl, html.Value);
                    Thread.Sleep(robotDelay * 1000);
                Dictionary<string, byte[]> others = crawler.GetSrcResourses(popUrl);
                // var r = crawler.GetAllaLinkWithHTMLFromUrl(baseLinks[0]);

                // crawler.SaveFiles(new Dictionary<string, string>({ html, html}), "C:\\"+new Uri(baseLinks[0]).Host)};
                if(csss.Any())
                    crawler.SaveResoursesToRoot(csss, FolderForSave + new Uri(popUrl).Authority); //Сохраняем ресурсы
                if(jss.Any())
                    crawler.SaveResoursesToRoot(jss, FolderForSave + new Uri(popUrl).Authority);
                if(others.Any())
                    crawler.SaveResoursesToRoot(others, FolderForSave + new Uri(popUrl).Authority);

                var htmlCode = crawler.UrlReplace(html.Value, html.Key); //Меняем ссылки в HTML докуменет
                if(html.Key != null)
                    crawler.SaveResoursesToRoot(new Dictionary<string, string>
                    {
                        {html.Key, htmlCode}
                    }, FolderForSave); //Сохраянем измененный хтмл
                // string st = crawler.GetHtmlFromUrl(baseLinks[0]);
            }
        }


        public Action<Object> SelectFolder()
        { 
            return s =>
            {
                var dialog = new FolderBrowserDialog();
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    FolderForSave = dialog.SelectedPath;
                }
            };
        }

        void Proc2(Object threads)
        {
            while(true)
            {
                CountThreads = ((threads)as List<Thread>).Count(el => el.ThreadState == ThreadState.Running);
                Thread.Sleep(5000);
            }
        }
        
        private List<Thread> threads;
        private Thread thr2;
        public Action<Object> StartCr()
        { 
            return s =>
            {
                baseLinks = TextBoxContent.Split('\r');

               threads = new List<Thread>();
                foreach(var item in baseLinks)
                {
                    threads.Add(new Thread(new ParameterizedThreadStart(Proc)));
                    threads.Last().Start(item);//передача параметра в поток
                } 
                thr2 = new Thread(new ParameterizedThreadStart(Proc2));
                thr2.Start(threads);

            };
        }
        public Action<Object> ThreadStop()
        {
            return s =>
            { 
                foreach(var item in threads)
                {
                   item.Abort();
                }
                CountThreads = ((threads) as List<Thread>).Count(el => el.ThreadState == ThreadState.Running);
                thr2.Abort();
            };
        }

    }
}