﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CsQuery;
using CsQuery.ExtensionMethods;
using CsQuery.Web;

namespace MvvmLight1.Crawler
{
    internal class Crawler
    {
        #region private 

        private Dictionary<string, string> BytesToString(Dictionary<string, byte[]> resourses)
        {
            if(!resourses.Any())
            {
               return new Dictionary<string, string>(); 
            }
            Dictionary<string,string> result = new Dictionary<string,string>(); 
            foreach (var item in resourses)
            {
                if(item.Value == null)
                {
                    continue;
                }
                result.Add(item.Key,Encoding.GetEncoding(1251).GetString(item.Value));
            }
            return result;
        }

        private Dictionary<string, byte[]> StringToBytes(Dictionary<string, string> files )
        {
            Dictionary<string, byte[]> result = new Dictionary<string, byte[]>();
            foreach (var item in files)
            {
                result.Add(item.Key,Encoding.GetEncoding(1251).GetBytes(item.Value));
            }
            return result;
        }

        private string CheckName(string name)
        {
            if(name.Length >= 50)
            {
                return name.Remove(0, name.Length - 50);
            }
            return name;
        }

        private byte[] GetStreamBytes(Stream stream)
        {
            byte[] buffer = new byte[16 * 1024];
            using(MemoryStream ms = new MemoryStream())
            {
                int read;
                while((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        private byte[] DownloadAnyFile(string imageUrl)
        {
             byte[] data = null;

            try
            {

                byte[] buffer;
                WebRequest req = WebRequest.Create(imageUrl);
                WebResponse response = req.GetResponse();
                using(Stream stream = response.GetResponseStream())
                {
                    data = GetStreamBytes(stream);
                }
                response.Close();
            }
            catch(Exception ex)
            {
                return null;
            }

            return data;
        }
 
        private void SaveAnyFile(string path,string fileName, byte[] content)
        {
            if(content==null)
            {
                return;
            }
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (!File.Exists(path + "\\" + fileName))
            {
                using (FileStream fstream = new FileStream(path + "\\" + fileName,FileMode.OpenOrCreate))
                { 
                    fstream.Write(content,0,content.Length);
                }
            }     
        }

        private Dictionary<string, byte[]> GetResourceFromUrl(String url, string element, string attr,
        string extension = "", string html = null)
        {
            url = ToLocalLink(url, url);
            #region initialization of variables. Get url List'

            string antiExt="";
            if(extension!="")
            {
                antiExt = extension[0] == '!' ? extension.Remove(0, 1) : "";
            }
            
            Dictionary<string,byte[]> resultList = new Dictionary<string,byte[]>();
            Uri uri = new Uri(url); //Сздаем ссылку
            var baseUrl = uri.Scheme + "://" + uri.Authority; //Корневая ссылка
            var cq = html != null ? CQ.CreateDocument(html) : CQ.CreateFromUrl(url);// Создаем dom
            var list = cq.Find(element).Select(obj => obj.GetAttribute(attr)).Where(el => !string.IsNullOrWhiteSpace(el) && el!="" && el.IndexOf(antiExt, StringComparison.Ordinal) != -1 && el.IndexOf(extension, StringComparison.Ordinal) != -1 && el != "#" && el != "/" && el.IndexOf('/') != -1 && el != url).Distinct().ToList();//фильтруем ссылки, которые не пустые и которые ссылаются на ресурс нужного формата
            #endregion

            foreach (var item in list)//Извлекаем ссыылки
            {
                string key = extension != "" ? ParamRemove(ToLocalLink(item, url)) : item;//Если расширение есть, то удаляем параметры после и приводим ссылку к нужному формату
                byte[] content = DownloadAnyFile(ToLocalLink(key,baseUrl)); //Скачиваем файл //HttpRequest(ToLocalLink(key, baseUrl));

                if (!resultList.ContainsKey(ToLocalLink(key, baseUrl)))//Если такого ключа еще нет
                {
                    resultList.Add(ToLocalLink(key, baseUrl), content);
                }
            }
            return resultList;
        }

        private string ToLocalLink(string url, string baseUrl="")
        {
            if(string.IsNullOrEmpty(url))
                return "";
            if( (url.IndexOf('/') > -1 && GetExtension(url.Split('/').First()) != "") || (url.IndexOf('/') == -1 && GetExtension(url) != ""))
            {//Если есть домен первого уровня. Значит ссылка URL
             
                    if(url.IndexOf("://", StringComparison.Ordinal) == -1)
                    {
                        url = "http://" + url;
                    }
            }

            if (url[0] == '.' && url[1] == '.')
            {
                string tempUrl = url;
                for (int i = 0; i < url.Length - 1; i++)
                {
                    if (url[i] == '.' && url[i + 1] == '.' && url[i + 2] == '/')
                    {//Удаляем точки
                        tempUrl = tempUrl.Remove(0, 3);
                    }
                }

                string[] resourses = url.Split('/').Where(el => el == "..").ToArray();
                string[] basesUrlArr = baseUrl.Split('/').ToArray();
                basesUrlArr = basesUrlArr.Where(el => el != new Uri(baseUrl).Scheme + ":" && (el != "" && GetExtension(el) == "")|| (el != "" && GetExtension(el) != ""&& basesUrlArr.IndexOf(el) != basesUrlArr.Length - 1)).ToArray();//Оствляет только папки (exmpl.com | rr |)

                var j = basesUrlArr.Length - 1;
                for (int i = resourses.Length-1; i != 0; i--)
                {//Удаляем лишние папки, чтобы переместиться в нужный каталог, туда, где 
                    basesUrlArr[j] = "";
                    j--;
                }

                return new Uri(baseUrl).Scheme + "://" + string.Join("", basesUrlArr.Where(el => el != "")) + "/"
                + tempUrl;
            }
            if (url[0] != '/')
            {
                return url;//"/" + new Uri(url).Authority + new Uri(url).AbsolutePath;
                           //url.Remove(0, url.IndexOf("/", url.IndexOf(":", StringComparison.Ordinal) + 3, StringComparison.Ordinal));
            }
            var t = url.Split('/').Where(el => el != "");
            if (t.First().IndexOf('.') != -1)
            {
                if (url[0] == '/')
                {
                    url = url.Remove(0, 1);
                    if (url[0] == '/')
                    {
                        url = url.Remove(0, 1);
                    }
                }
                return new Uri(baseUrl).Scheme + "://" + url;
            }
            if (url[0] == '/' && url[1] == '/')
            {
                return url.Remove(0, 1);
                //url.Remove(0, url.IndexOf("/", url.IndexOf("/", StringComparison.Ordinal) + 3, StringComparison.Ordinal));
            }
            if (url[0] == '/' && url[1] != '/')
            {
                return new Uri(baseUrl).Scheme + "://" + new Uri(baseUrl).Host + url;
            }
            return url;
        }

        private string GetExtension(string path)
        {
            if(string.IsNullOrEmpty(path))
            {
                return "";
            }
            string[] files = path.Split('/').Where(el => el != "").ToArray();
            string file = files.Last();
            if (file.IndexOf('.') > -1)
                return file.Remove(0, file.LastIndexOf('.') + 1);
            return "";
        }

        [Serializable]
        private class MyException : ApplicationException
        {
            public MyException()
            {
            }

            public MyException(string message) : base(message)
            {
            }

            public MyException(string message, Exception ex) : base(message)
            {
            }

            // Конструктор для обработки сериализации типа
            protected MyException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext contex) : base(info, contex)
            {
            }
        }

        private string ParamRemove(string href)
        {
            try
            {
                if(href.IndexOf('?') > -1)
                {
                    string[] arr = href.Split('/').ToArray();
                    string temp = arr.Last();
                    arr[arr.Length - 1] = temp.Remove(temp.IndexOf('?'));
                    href = string.Join("/", arr);
                }
            }
            catch
            {
                return "";
            }
            return href;
        }

        private string HtmlModif(string href, string htmlUrl)
        {

            href = ParamRemove(href);//Удаляем параметры из ссылки, если есть
            if (string.IsNullOrEmpty(href))
            {
                return "";
            }
            string[] htmlUrlArr = htmlUrl.Split('/').ToArray();
            //if(GetExtension(href.Split('/').Last(el => !string.IsNullOrEmpty(el))) != "")//Если есть расширение
            //{
                string[] hrefstemp = href.Split('/').Where(el => !string.IsNullOrEmpty(el)).ToArray();

                hrefstemp[hrefstemp.Length - 1] = CheckName(hrefstemp.Last());//Если нужно укорачиваем имя
            //}
            htmlUrlArr = htmlUrlArr.Where(el =>el != new Uri(htmlUrl).Scheme + ":" && (el != "" && GetExtension(el) == "")|| (el != "" && GetExtension(el) != "" && htmlUrlArr.IndexOf(el) != htmlUrlArr.Length - 1)).ToArray();

            string resultHref = "";
            
            for (int i = 0; i < htmlUrlArr.Length - 1; i++)
            {//Делаем переходы назад, чтобы перейти в корень
                resultHref += "../";
            }
            var extension = GetExtension(href);
            if (extension == "css")
            {
                resultHref += "Styles/" + href.Split('/').Last(el => el != "");
            }
            else if (extension == "js")
            {
                resultHref += "Scripts/" + href.Split('/').Last(el => el != "");
            }
            else if(extension == "html" || extension == "htm" || extension == "aspx" || extension == "php")
            {
                resultHref += "Others/" + href.Split('/').Last(el=>el != "");
            }
            //else if(htmlUrlArr.Length==1) //Это домент например exam.com
            //{
            //    resultHref
            //}

            return resultHref;
        }
        #endregion


        #region public

        public int GetTimeRobotTxt(string url)
        {
            if(string.IsNullOrEmpty(url))
            {
                return 0;
            }

            url = ToLocalLink(url);//Приводим URL к нормальному виду
            url = new Uri(url).Scheme + "://" + new Uri(url).Authority;
            byte[] robotArr = null;
            try
            {
                robotArr = DownloadAnyFile(url + "/robots.txt");
            }
            catch
            {
                return 0;
            }

            var robot = Encoding.GetEncoding(1251).GetString(robotArr);
            int crawlDelay = robot.IndexOf("Crawl-delay:", StringComparison.Ordinal);
            string delay = "0";

            for(int i = crawlDelay+ "Crawl-delay:".Length; i < robot.Length; i++)
            {
                if(char.IsDigit(robot[i]))
                {
                    delay += robot[i];
                }else if(delay != "" && !char.IsDigit(robot[i]))
                {
                    break;
                }
            }
            return int.Parse(delay);
        }
        public Dictionary<string, byte[]> GetSrcResourses(string url, string html = null)
        {
            var one = GetResourceFromUrl(url, "img", "src", "", html );
            var two = GetResourceFromUrl(url, "link", "href", "!css", html);

            foreach(var item in one)
            {
                two.Add(item.Key, item.Value);
            }
            return two;
        }

        public void SaveResoursesToRoot(Dictionary<string, string> data, string path)
        {
            SaveResoursesToRoot(StringToBytes(data), path);
        }

        public void SaveResoursesToRoot(Dictionary<string, byte[]> data, string path)
        {
            string resourseFolder = "";
            string extension = GetExtension(data.Keys.First());

            #region check extension

            if(data.All(el=>GetExtension(el.Key) != extension)) //Проверяем все ли элементы одного разширения
            {
                MyException exc = new MyException("В полученном списке файлы с разным расширением");
                throw exc;
            }

            #endregion

            foreach(var item in data)
            {
                if(extension == "css")
                {
                    resourseFolder = "Styles";

                }
                else if(extension == "js")
                {
                    resourseFolder = "Scripts";
                }
                else if(extension == "html" || extension == "htm" || extension == "aspx" || extension == "php")
                {
                    var t = item.Key.Split('/').ToArray();
                    resourseFolder = string.Join("/", t.Where(el=>el != new Uri(item.Key).Scheme + ":" && (el != "" && GetExtension(el) == "")|| (el != "" && GetExtension(el) != "" && t.IndexOf(el) != t.Length - 1)).ToArray());
                }
                else
                {
                    resourseFolder = "Others";
                }

                string file = item.Key;
                string scheme = new Uri(item.Key).Scheme;
                if(item.Key.IndexOf(scheme, StringComparison.Ordinal) != -1)
                {
                    file = item.Key.Remove(0, scheme.Length + 3);
                }
                // var rootFolder = file.Split('/').Where(el => el != "").ToArray().First() + "\\"+ resourseFolder;//Папка, в которой будет находится стиль: имя домена и папка связанная с ресурсом
                var fileName = ParamRemove(file.Split('/').Where(el=>el != "").ToArray().Last());
                SaveAnyFile(path + "/" + resourseFolder, fileName, item.Value); //Создаем папки, сохраняем сожержимое
            }

        }

        #region old Method
        //public void SaveFileToRoot(Dictionary<string, string> data, string path)
        //{
        //    string resourseFolder = "";
        //    string extension = GetExtension(data.Keys.First());

        //    #region check extension

        //    if (data.All(el => GetExtension(el.Key) != extension)) //Проверяем все ли элементы одного разширения
        //    {
        //        MyException exc = new MyException("В полученном списке файлы с разным расширением");
        //        throw exc;
        //    }

        //    #endregion

        //    foreach (var item in data)
        //    {

        //        switch (extension)
        //        {
        //            case "css":
        //                resourseFolder = "Styles";
        //                break;
        //            case "js":
        //                resourseFolder = "Scripts";
        //                break;
        //            default:
        //                if (GetExtension(item.Key) != "")
        //                {
        //                    var t = item.Key.Split('/').ToArray();
        //                    resourseFolder = string.Join("/", t.Where(el => el != new Uri(item.Key).Scheme + ":" && (el != "" && GetExtension(el) == "")
        //                    || (el != "" && GetExtension(el) != "" && t.IndexOf(el) != t.Length - 1))
        //                    .ToArray());
        //                }
        //                break;
        //        }

        //        string file = item.Key;
        //        string scheme = new Uri(item.Key).Scheme;
        //        if (item.Key.IndexOf(scheme, StringComparison.Ordinal) != -1)
        //        {
        //            file = item.Key.Remove(0, scheme.Length + 3);
        //        }
        //        // var rootFolder = file.Split('/').Where(el => el != "").ToArray().First() + "\\"+ resourseFolder;//Папка, в которой будет находится стиль: имя домена и папка связанная с ресурсом
        //        var fileName = file.Split('/').Where(el => el != "").ToArray().Last();
        //        SaveFile(path + "\\" + resourseFolder, fileName, item.Value); //Создаем папки, сохраняем сожержимое
        //    }
        //}

        //public void SaveFiles(Dictionary<string, string> data, string path)
        //{
        //    foreach(var item in data)
        //    {
        //        string[] directories = item.Key.Split('/').Where(el => el != "").ToArray();
        //        string localPath = "";
        //        for(int i = 0; i < directories.Count() - 1; i++)
        //        {
        //            localPath += @"\" + directories[i];
        //        }

        //        SaveFile(path + localPath, directories[directories.Count() - 1], item.Value);
        //        //Сраняем указанный файл, по указанному пути, с указанным содержимым

        //        #region SaveFile Old

        //        //if (!Directory.Exists(path + localPath))
        //        //{
        //        //    Directory.CreateDirectory(path + localPath);
        //        //}
        //        //if(!File.Exists(path + localPath + "\\" + directories[directories.Count() - 1]))
        //        //{
        //        //    using(File.Create(path + localPath + "\\" + directories[directories.Count() - 1]))
        //        //    {

        //        //    } 
        //        //}


        //        //using (FileStream fstream = new FileStream(path+ localPath + "\\" + directories[directories.Count() - 1], FileMode.OpenOrCreate))
        //        //{ 
        //        //    byte[] array = System.Text.Encoding.Default.GetBytes(item.Value); 
        //        //    fstream.Write(array, 0, array.Length);
        //        //}

        //        #endregion
        //    }
        //}

        #endregion

        /// <summary>
        /// Возращает все сылки, найденные в HTML документе
        /// </summary>
        /// <param name="link">Ссылка на HTML документ </param>
        /// <returns>Список ссылок</returns>
        public Dictionary<string,string> GetAllaLinkWithHTMLFromUrl(String link)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            var resourses = GetResourceFromUrl(link,"a","href");
            foreach (var item in resourses)
            {
                result.Add(item.Key,Encoding.GetEncoding(1251).GetString(item.Value));
            }

            return result;
        }

        /// <summary>
        /// Изменяет ссылки ресурсов в html документе в соотвествии с расположением html документа
        /// </summary>
        /// <param name="html">Документ html</param>
        /// <param name="url">Ссылка на документ</param>
        /// <returns></returns>
        public string UrlReplace(string html, string url)
        {
            CQ cq = CQ.CreateDocument(html); //Получаем html 
         
                cq.Find("base").Where(el =>
                       el.GetAttribute("href") != null && el.GetAttribute("href") != "#"
                           && el.GetAttribute("href") != "/").ForEach(el => el.SetAttribute("href", ""));
                cq.Find("link").Where(el => /*el.GetAttribute("href")== "stylesheet" &&*/
                       el.GetAttribute("href") != null && el.GetAttribute("href") != "#"
                           && el.GetAttribute("href") != "/").ForEach(
                                el => el.SetAttribute("href", HtmlModif(el.GetAttribute("href"), url)));
                cq.Find("script").Where(el =>
                       el.GetAttribute("src") != null && el.GetAttribute("src") != "#" && el.GetAttribute("src") != "/")
                    .ForEach(el => el.SetAttribute("src", HtmlModif(el.GetAttribute("src"), url)));
                cq.Find("img").Where(el =>
                    el.GetAttribute("src") != null && el.GetAttribute("src") != "#" && el.GetAttribute("src") != "/")
                 .ForEach(el => el.SetAttribute("src", HtmlModif(el.GetAttribute("src"), url)));

            return cq.Document.Render();
        }
        /// <summary>
        /// Возвращает HTML документ, расположенный по указанному адресу
        /// </summary>
        /// <param name="url">Адрес документа</param>
        /// <param name="replaceFlag">Изменять ли ссылки на ресурсы, в соответствии с расположением документа</param>
        /// <returns>Ссылка на документ, HTML разметка</returns>
        public KeyValuePair<string, string> GetHtmlFromUrl(string url, bool replaceFlag=true)
        {
            CQ cq = CQ.CreateFromUrl(url); //Получаем html 
            var html = cq.Document.Render();
            if(replaceFlag)
            {
                html = UrlReplace(html, url);
            }
         
            //cq.Map(el=>el.)
            string bodyUrl = new Uri(url).LocalPath;
            if(GetExtension(url) =="" || url[url.Length-1]=='/')
            {
                if(bodyUrl[bodyUrl.Length-1]!='/')
                bodyUrl += "/index.html";
                else
                {
                    bodyUrl += "index.html";
                }
            } 
            if(html != "")
            {
                return new KeyValuePair<string, string>(new Uri(url).Scheme+"://" + new Uri(url).Host + bodyUrl, html);
            }
                return new KeyValuePair<string,string>("", null);

        }

        public List<string> GetLinksFromUrl(string link)
        {
            link = ToLocalLink(link);
            Uri url = new Uri(link); //Получаем ссылку
            var baseUrl = url.Scheme + "://" + url.Authority; //Корневая ссылка
            CQ cq;

            #region подключаемся по URL ловим исключения

            try
            {
                cq = CQ.CreateFromUrl(link); //Получаем html
            }
            catch
            {
                Thread.Sleep(3000);
                try
                {
                    cq = CQ.CreateFromUrl(link); //Пытаемся ещё раз получить html
                }
                catch(WebException ex)
                {
                    throw ex;
                }
            }

            #endregion

            List<string> resultList = new List<string>(); //Тут будем хранить все ссылки, которые нужно вернуть

            var list =
                cq.Find("a").Select(obj => obj.GetAttribute("href")).Where(
                    el =>
                        el != null && el != "#" && el != "/" /*&& el.IndexOf('.')!=-1*/&& el.IndexOf('/') != -1
                            && el != link && !string.IsNullOrEmpty(el)).Distinct().ToList();
                //Элементы а, извлекаем атрибуты href, фильтруем результат

            list.ForEach(el =>
            {
                if(el[0] == '/' && el[1] != '/' && el.IndexOf('.') == -1)
                    //Если первый символ /, то в начало добавляем корневую ссылку
                {
                    resultList.Add(baseUrl + el);
                }
                else
                {
                    resultList.Add(el);
                } 
            });

            List<string> newList = new List<string>();
            foreach(var item in resultList)
            {
                newList.Add(ToLocalLink(item, baseUrl));
            } 
            return newList;
        }

        public Dictionary<string, string> GetCssFromUrl(string url, string html=null)
        {
            return BytesToString(GetResourceFromUrl(url, "link", "href", ".css", html));

        }

        public Dictionary<string,string> GetJsFromUrl(string url, string html=null)
        {
            return BytesToString(GetResourceFromUrl(url,"script","src",".js",html));
        }

        public List<string> Links { get; set; }

        #endregion
    }
}









      